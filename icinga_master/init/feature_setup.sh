#!/bin/bash
# Enable Icinga2 Features
set -e

ENABLED_FEATURES=$(icinga2 feature list | grep "Enabled features" | cut -d: -f 2)

for f in ${ICINGA_FEATURES} ; do
  echo ${ENABLED_FEATURES} | grep api -q && continue; # feature already enabled

  icinga2 feature enable ${f}
done
